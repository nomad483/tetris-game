let overlay = document.querySelector('.overlay'); //ищем элемент overlay
let modal = document.querySelector('.modal'); //ищем элемент modal
let speed = 0; //инициализируем переменную speed, присваиваем 0

modal.addEventListener('click', function (e) { // при клике на modal запускается пункция с ивентом
  if (e.target.classList.contains('easy')) { //проверка ивента - класс элемента modal
    speed = 1000;
  } else if (e.target.classList.contains('normal')) {//проверка ивента - класс элемента modal
    speed = 500;
  } else if (e.target.classList.contains('hard')) {//проверка ивента - класс элемента modal
    speed = 200;
  }

  if (e.target.classList.contains('button')) { //при клике на элемент с классом button
    modal.style.display = 'none'; //скрываем элемент modal
    overlay.style.display = 'none'; //скрываем элемент overlay
    startGame(); // запуск функции запуска игры
  }
});

function startGame() { //функция запуска игры

  let tetris = document.createElement('div'); // создаем блок, записываем в переменную tetris
  tetris.classList.add('tetris'); // добавляем класс tetris

  for (let i = 0; i < 180; i++) {
    let excel = document.createElement('div'); // создаем блок
    excel.classList.add('excel'); // добавляем класс excel
    tetris.appendChild(excel); // добавляем к блоку с классом tetris  блок-клетку с классом excel
  } // цикл прорисовки сетки в 180 клеток (18 строк х 10 столбцов)

  let main = document.querySelector('.main'); // находим элемент с классом main, записываем в переменную main
  main.appendChild(tetris); // к блоку main добавляем сетку

  let excel = document.querySelectorAll('.excel'); //находим элемент с классом excel, записываем в переменную excel
  let i = 0; // создаем переменную i, присваиваем значение 0

  for (let y = 17; y >= 0; y--) { // перебор сетки по оси y (строки)
    for (let x = 0; x < 10; x++) { // перебор сетки по оси x (столбцы)
      excel[i].setAttribute('posX', x); // присваиваем клетки значение по оси x
      excel[i].setAttribute('posY', y); // присваиваем клетки значение по оси y
      i++; // инкремент переменной i
    }
  } // цикл переборки сетки

  let x = 4, y = 14; //объявляем переменные x и y, присваиваем им значения, точка отрисовки фигур
  let mainArr = [
    [ //палка
      [0, 1],
      [0, 2],
      [0, 3],
      [
        [-1, 1],
        [0, 0],
        [1, -1],
        [2, -2]
      ],
      [
        [1, -1],
        [0, 0],
        [-1, 1],
        [-2, 2]
      ],
      [
        [-1, 1],
        [0, 0],
        [1, -1],
        [2, -2]
      ],
      [
        [1, -1],
        [0, 0],
        [-1, 1],
        [-2, 2]
      ]
    ],

    [ //квадрат
      [1, 0],
      [0, 1],
      [1, 1],
      [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0]
      ],
      [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0]
      ],
      [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0]
      ],
      [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0]
      ]
    ],

    [ //L - образная
      [1, 0],
      [0, 1],
      [0, 2],
      [
        [0, 0],
        [-1, 1],
        [1, 0],
        [2, -1]
      ],
      [
        [1, -1],
        [1, -1],
        [-1, 0],
        [-1, 0]
      ],
      [
        [-1, 0],
        [0, -1],
        [2, -2],
        [1, -1]
      ],
      [
        [0, -1],
        [0, -1],
        [-2, 0],
        [-2, 0]
      ]
    ],

    [ // обратная L
      [1, 0],
      [1, 1],
      [1, 2],
      [
        [0, 0],
        [0, 0],
        [1, -1],
        [-1, -1]
      ],
      [
        [0, -1],
        [-1, 0],
        [-2, 1],
        [1, 0]
      ],
      [
        [2, 0],
        [0, 0],
        [1, -1],
        [1, -1]
      ],
      [ //y x
        [-2, 0],
        [1, -1],
        [0, 0],
        [-1, 1]
      ]
    ],

    [ // обратная z
      [1, 0],
      [1, 1],
      [2, 1],
      [
        [2, -1],
        [0, 0],
        [1, -1],
        [-1, 0]
      ],
      [
        [-2, 0],
        [0, -1],
        [-1, 0],
        [1, -1]
      ],
      [
        [2, -1],
        [0, 0],
        [1, -1],
        [-1, 0]
      ],
      [
        [-2, 0],
        [0, -1],
        [-1, 0],
        [1, -1]
      ]
    ],

    [ // z фигура
      [1, 0],
      [-1, 1],
      [0, 1],
      [
        [0, -1],
        [-1, 0],
        [2, -1],
        [1, 0]
      ],
      [
        [0, 0],
        [1, -1],
        [-2, 0],
        [-1, -1]
      ],
      [
        [0, -1],
        [-1, 0],
        [2, -1],
        [1, 0]
      ],
      [
        [0, 0],
        [1, -1],
        [-2, 0],
        [-1, -1]
      ]
    ],

    [ // перевернутая Т
      [1, 0],
      [2, 0],
      [1, 1],
      [
        [1, -1],
        [0, 0],
        [0, 0],
        [0, 0]
      ],
      [
        [0, 0],
        [-1, 0],
        [-1, 0],
        [1, -1]
      ],
      [
        [1, -1],
        [1, -1],
        [1, -1],
        [0, 0]
      ],
      [
        [-2, 0],
        [0, -1],
        [0, -1],
        [-1, -1]
      ]
    ]
  ];

  let currentFigure = 0;
  let figureBody = 0;
  let rotate = 1;

  function create() {
    function getRandom() { //функция рандомного выбора фигур из массива
      return Math.round(Math.random() * (mainArr.length - 1))
    }

    rotate = 1;
    currentFigure = getRandom(); // присваиваем переменной фигуру

    figureBody = [ //отрисовка фигуры по заданым координатам
      document.querySelector(`[posX = "${x}"][posY = "${y}"]`),
      document.querySelector(`[posX = "${x + mainArr[currentFigure][0][0]}"][posY = "${y + mainArr[currentFigure][0][1]}"]`),
      document.querySelector(`[posX = "${x + mainArr[currentFigure][1][0]}"][posY = "${y + mainArr[currentFigure][1][1]}"]`),
      document.querySelector(`[posX = "${x + mainArr[currentFigure][2][0]}"][posY = "${y + mainArr[currentFigure][2][1]}"]`)
    ];

    for (let i = 0; i < figureBody.length; i++) {
      figureBody[i].classList.add('figure'); // присваиваем каждому элементу класс figure
    }
  }

  create();

  let score = 0;
  let rezult = document.querySelector('.rezult');
  console.log(rezult);
  rezult.value = `Ваши очки: ${score}`;

  function move() {
    let moveFlag = true;
    let coordinates = [
      [figureBody[0].getAttribute('posX'), figureBody[0].getAttribute('posY')],
      [figureBody[1].getAttribute('posX'), figureBody[1].getAttribute('posY')],
      [figureBody[2].getAttribute('posX'), figureBody[2].getAttribute('posY')],
      [figureBody[3].getAttribute('posX'), figureBody[3].getAttribute('posY')]
    ];

    for (let i = 0; i < coordinates.length; i++) {
      if (coordinates[i][1] == 0 || document.querySelector(`[posX = "${coordinates[i][0]}"][posY = "${coordinates[i][1] - 1}"]`).classList.contains('set')) {
        moveFlag = false;
        break;
      }
    }

    if (moveFlag) {
      for (let i = 0; i < figureBody.length; i++) {
        figureBody[i].classList.remove('figure');
      }
      figureBody = [
        document.querySelector(`[posX = "${coordinates[0][0]}"][posY = "${coordinates[0][1] - 1}"]`),
        document.querySelector(`[posX = "${coordinates[1][0]}"][posY = "${coordinates[1][1] - 1}"]`),
        document.querySelector(`[posX = "${coordinates[2][0]}"][posY = "${coordinates[2][1] - 1}"]`),
        document.querySelector(`[posX = "${coordinates[3][0]}"][posY = "${coordinates[3][1] - 1}"]`)
      ];
      for (let i = 0; i < figureBody.length; i++) {
        figureBody[i].classList.add('figure');
      }
    } else {
      for (let i = 0; i < figureBody.length; i++) {
        figureBody[i].classList.remove('figure');
        figureBody[i].classList.add('set');
      }
      for (let i = 0; i < 14; i++) {
        let count = 0;
        for (let k = 0; k < 10; k++) {
          if (document.querySelector(`[posX = "${k}"][posY = "${i}"]`).classList.contains('set')) {
            count++;
            if (count === 10) {
              score += 10;
              rezult.value = `Ваши очки: ${score}`;
              for (let m = 0; m < 10; m++) {
                document.querySelector(`[posX = "${m}"][posY = "${i}"]`).classList.remove('set')
              }
              let set = document.querySelectorAll('.set');
              let newSet = [];
              for (let s = 0; s < set.length; s++) {
                let setCoordinates = [set[s].getAttribute('posX'), set[s].getAttribute('posY')];
                if (setCoordinates[1] > i) {
                  set[s].classList.remove('set');
                  newSet.push(document.querySelector(`[posX = "${setCoordinates[0]}"][posY = "${setCoordinates[1] - 1}"]`));
                }
              }
              for (let a = 0; a < newSet.length; a++) {
                newSet[a].classList.add('set');
              }
              i--;
            }
          }
        }
      }
      for (let n = 0; n < 10; n++) {
        if (document.querySelector(`[posX = "${n}"][posY = "14"]`).classList.contains('set')) {
          clearInterval(interval);
          alert(`Игра окончена, ваш результат: ${score}`);
          break;
        }
      }
      create();
    }
  }

  let interval = setInterval(() => {
    move();
  }, speed);

  let flag = true;

  window.addEventListener('keydown', function (e) {
    let coordinates1 = [figureBody[0].getAttribute('posX'), figureBody[0].getAttribute('posY')];
    let coordinates2 = [figureBody[1].getAttribute('posX'), figureBody[1].getAttribute('posY')];
    let coordinates3 = [figureBody[2].getAttribute('posX'), figureBody[2].getAttribute('posY')];
    let coordinates4 = [figureBody[3].getAttribute('posX'), figureBody[3].getAttribute('posY')];

    function getNewState(a) {

      flag = true;

      let figureNew = [
        document.querySelector(`[posX = "${+coordinates1[0] + a}"][posY = "${coordinates1[1]}"]`),
        document.querySelector(`[posX = "${+coordinates2[0] + a}"][posY = "${coordinates2[1]}"]`),
        document.querySelector(`[posX = "${+coordinates3[0] + a}"][posY = "${coordinates3[1]}"]`),
        document.querySelector(`[posX = "${+coordinates4[0] + a}"][posY = "${coordinates4[1]}"]`)
      ];

      for (let i = 0; i < figureNew.length; i++) {
        if (!figureNew[i] || figureNew[i].classList.contains('set')) {
          flag = false;
        }
      }

      if (flag) {
        for (let i = 0; i < figureBody.length; i++) {
          figureBody[i].classList.remove('figure');
        }

        figureBody = figureNew;

        for (let i = 0; i < figureBody.length; i++) {
          figureBody[i].classList.add('figure');
        }
      }
    }

    if (e.key === 'ArrowLeft') {
      getNewState(-1);
    } else if (e.key === 'ArrowRight') {
      getNewState(1);
    } else if (e.key === 'ArrowDown') {
      move();
    } else if (e.key === 'ArrowUp') {

      flag = true;

      let figureNew = [
        document.querySelector(`[posX = "${+coordinates1[0] + mainArr[currentFigure][rotate + 2][0][0]}"][posY = "${
          +coordinates1[1] + mainArr[currentFigure][rotate + 2][0][1]}"]`),
        document.querySelector(`[posX = "${+coordinates2[0] + mainArr[currentFigure][rotate + 2][1][0]}"][posY = "${
          +coordinates2[1] + mainArr[currentFigure][rotate + 2][1][1]}"]`),
        document.querySelector(`[posX = "${+coordinates3[0] + mainArr[currentFigure][rotate + 2][2][0]}"][posY = "${
          +coordinates3[1] + mainArr[currentFigure][rotate + 2][2][1]}"]`),
        document.querySelector(`[posX = "${+coordinates4[0] + mainArr[currentFigure][rotate + 2][3][0]}"][posY = "${
          +coordinates4[1] + mainArr[currentFigure][rotate + 2][3][1]}"]`)
      ];

      for (let i = 0; i < figureNew.length; i++) {
        if (!figureNew[i] || figureNew[i].classList.contains('set')) {
          flag = false;
        }
      }

      if (flag) {
        for (let i = 0; i < figureBody.length; i++) {
          figureBody[i].classList.remove('figure');
        }

        figureBody = figureNew;

        for (let i = 0; i < figureBody.length; i++) {
          figureBody[i].classList.add('figure');
        }

        if (rotate < 4) {
          rotate++;
        } else {
          rotate = 1;
        }
      }
    }
  });
}






